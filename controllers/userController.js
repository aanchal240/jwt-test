
const User=require('../model/users')
const service=require('../services/userService')
const common=require('../utils/common')

module.exports.getAllUsers=async (req,res)=>
   {
       //service.saveUser();
       try{
       const result=await service.createUser()
       //res.send(result)
       //res.status(201).send(result)
       common.sendSuccess(res,result)
       }
       catch(error)
       {
         //res.status(404).send("error:"+ error.message)
         common.sendError(res,error.message)
       }
      // console.log(data)
   }
module.exports.createUsers=async(req,res)=>{
   try{
      const user=req.body
      console.log(user)
  await service.saveUser(user)
   res.status(201).send("user created")
   }
   catch(error)
   {
      res.status(404).send(error)
   }
}
module.exports.updateUsers=async (req,res)=>{
   try{
     const user_id=req.params.id
    const user_name=req.body.name;
    // console.log(user_name)
   const updated_data=await service.updateUser(user_id,user_name)
   res.status(201).send("updated")
   }
   catch(error)
   {
      res.status(404).send(error.message)
   }
}
module.exports.deleteUsers=async (req,res)=>{
   try{
   const id = req.params.id;
  const delted=await service.deleteUser(id)
  res.status(201).send("deleted")
   }
   catch(error)
   {
    res.status(404).send("error:"+ error.message)
   }
}
module.exports.signupUser=async(req,res)=>{
   try{
     const profile=await service.signup(req.body)
     common.sendSuccess(res,profile)
   }
   catch(err)
   {
      common.sendError(res,err.message)
   }
}
module.exports.signinUser=async(req,res)=>{
   try{
      
      const result=await service.signin(req.body.email,req.body.password)
      common.sendSuccess(res,result)
   }
   catch(err)
   {
      common.sendError(res,err.message)
   }
}
module.exports.profile=async(req,res)=>{
   try{
      const user=await service.profile(req.user)
      common.sendSuccess(res,user)
   }
   catch(err)
   {
      common.sendError(res,err.message)
   }
}

module.exports.logout=async(req,res)=>{
   try{
      const user=await service.logOut(req)
      common.sendSuccess(res,user)
   }
   catch(err)
   {
      common.sendError(res,err.message)
   }
}
module.exports.logoutAll=async(req,res)=>{
   try{
      const user=await service.logOutAll(req)
      common.sendSuccess(res,user)
   }
   catch(err)
   {
      common.sendError(res,err.message)
   }
}

// const main=async()=>{
//    const user=await User.findById('61e0feddb32da03f4cabc469')
//    await user.populate('last_name')
//    console.log(user.last_name)
// }
// main()

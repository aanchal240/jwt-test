const note=require('../model/note.model')
const service=require('../services/noteService')
const common=require('../utils/common')
console.log(note.title)
module.exports.createNotes=async (req,res)=>
   {
       //service.saveUser();
       try{
        const title=req.body.title
        const content=req.body.content
       const result=await service.saveNote(title,content)
       //res.send(result)
       //res.status(201).send(result)
       common.sendSuccess(res,"Note created")
       }
       catch(error)
       {
         //res.status(404).send("error:"+ error.message)
         common.sendError(res,error.message)
       }
      // console.log(data)
   }
module.exports.updateNotes=async(req,res)=>{
    try{
        const id=req.params.id
        const update=req.body
        const result=await service.updateNotes(id,update)
       
        common.sendSuccess(res,"Note updated");
    }catch(error)
    {
        common.sendError(res,error.message)
    }
}   
module.exports.leanNotes=async(req,res)=>{
    try{
        const id=req.params.id
        const result=await service.Note(id)
        console.log(result)
    }
    catch(error)
    {
        common.sendError(res,error.message)
    }
}
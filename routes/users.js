const express=require('express')

const router=new express.Router();

const userController=require('../controllers/userController')
const auth=require('../middleware/auth')
// console.log('hh')
router.get('/users',userController.getAllUsers)
router.post('/users',userController.createUsers)
router.put('/users/:id',userController.updateUsers)
router.delete('/users/:id',userController.deleteUsers)
router.post('/users/signup',userController.signupUser)
router.post('/users/signin',userController.signinUser)
router.get('/users/profile',auth,userController.profile)
router.post('/users/logout',auth,userController.logout)
router.post('/users/logoutAll',auth,userController.logoutAll)
module.exports=router
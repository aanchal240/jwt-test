const express=require('express')

const router=new express.Router();

router.use('/',require('./users'))
router.use('/',require('./notes'))

module.exports=router
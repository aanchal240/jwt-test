const express=require('express')

const router=new express.Router()

const noteController=require('../controllers/noteController')

//get all notes
// router.get('/notes',noteController.getAllnotes)

//create notes
router.post('/notes',noteController.createNotes)

//update notes with id
router.put('/notes/:id',noteController.updateNotes)

router.get('/notes/:id',noteController.leanNotes)

//delete notes
// router.delete('/notes/:id',noteController.deleteUsers)

module.exports=router
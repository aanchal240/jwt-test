require('dotenv').config()
const jwt=require('jsonwebtoken')
const User=require('../model/users')
const common=require('../utils/common')

const auth=async(req,res,next)=>{
  try{
    const token=req.header('Authorization').replace('Bearer ','')
    const decoded=jwt.verify(token,process.env.JWT_KEY)
    const user=await User.findOne({_id:decoded._id,'tokens.token':token})
    
    if(!user)
    {
        throw new Error()
    }
   req.token=token
    req.user=user
   
    next()

  }
  catch(error)
  {
     common.sendError(res,'Please authenticate.')
  }
}
module.exports=auth
const User=require('../model/users')

module.exports.saveUser=async(data)=>
{
    const user = new  User({
       name:data.name,
       email:data.email,
       age:data.age,
       address:data.address,
       password:data.password
    })
    await user.save()
}
module.exports.createUser=async()=>
{
      const data=await User.find({})
      //.log(data)
      if(data.length===0)
      {
          throw new Error("cannnot get users")
      }
      
      return data

}
       
module.exports.updateUser=async(user_id,user_name)=>{
    // const user=await User.findByIdAndUpdate(user_id,{name:user_name},function(err, result){
    //     if(err){
    //         console.log(err);
    //     }
    //      service.saveUser()
        
    //     return user
    //   });
    const user=await User.findById(user_id)
    
        
    if(!user)
    {
        throw new Error("invalid user")
    }
    user.name=user_name
    user.save()
     
}
module.exports.deleteUser=async(id)=>{
//    const user= await User.findByIdAndDelete(id, function (err, docs) {
//         if (!user){
//             throw new Error("user doesn't exit in database")
//         }
//         else{
//             console.log("Deleted : ", docs);
//             return user
//         }
//       })
     const user=await User.findById(id)
     if(!user)
     {
        throw new Error("user doesn't exit in database")
     }
    await user.remove()
}                      
module.exports.signup=async(data)=>{
    const user=await User.save(data)
    const token=await user.generateAuthToken()
    return {user,token}
    //return user
}
module.exports.signin=async(email,password)=>{
    
    const user=await User.findByCredentials(email,password)
    const token=await user.generateAuthToken()
    return {user,token}
      
}
module.exports.logOut=async(req)=>{

    console.log(req.user.tokens)
    req.user.tokens=req.user.tokens.filter((token)=>{
        return token.token!==req.token
    })

    await req.user.save()
    return req.user
}
module.exports.logOutAll=async(req)=>{
    req.user.tokens=[]
    await req.user.save()
    return req.user
} 
module.exports.profile=async(user)=>{
    return user
}
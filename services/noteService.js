const Note=require('../model/note.model')

module.exports.saveNote=async(title,content)=>{
    if(!content)
    {
        throw new Error({error : "Note content can not be empty"})   
    }
    const note=new Note({
       title,
       content
    })
    return await note.save()
    
}
module.exports.updateNotes=async(id,update)=>{
   const data= await Note.findOneAndUpdate(id, update, {upsert: true}, function(err, doc) {
        if (err) 
        throw new error({error:'Note cannot be updated'})
        return doc
    });
}
module.exports.Lean_Note=async(id)=>{
    const leanDoc = await Note.findOne().lean();
    return leanDoc
}
module.exports.Note=async(id)=>{
    const leanDoc = await Note.findOne()
    return leanDoc
}
// module.exports.create=async(title,content)=>{
//     if(!content)
//     {
//         throw new Error({error : "Note content can not be empty"})
//     }
//     note.save()
// }
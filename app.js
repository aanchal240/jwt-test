require('dotenv').config()
const express=require('express')

require('./connection/mongoose')

const app=express()

app.use(express.json())

app.use('/',require('./routes/index'))
app.use('/',require('./routes/notes'))

const port=process.env.PORT

app.listen(port,()=>{
    console.log('server is up on port.'+ port)
})
